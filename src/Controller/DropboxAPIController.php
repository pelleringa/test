<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use League\Flysystem\Filesystem;
use Srmklive\Dropbox\Client\DropboxClient;
use Srmklive\Dropbox\Adapter\DropboxAdapter;
use App\Entity\DropboxConfig;

class DropboxAPIController
{
    /**
     * @Route("/")
     */
    public function foldersList(Request $request)
    {
        $dropboxConfig = new DropboxConfig();//->getToken();
        $authorizationToken = $dropboxConfig->getToken();
        $client = new DropboxClient($authorizationToken);
        $adapter = new DropboxAdapter($client);
        $filesystem = new Filesystem($adapter);
        $folder = $request->query->get('folder');
        $files = $adapter ->listContents($folder);
        $links = [];

        foreach ($files as $file) {
            $links[] = array('name' => $file['path'], 'link' => $adapter->getTemporaryLink('/' . $file['path']));
        }

        return new JsonResponse($links);
    }
}